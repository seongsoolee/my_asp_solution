﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.IO;
using log4net;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

public partial class sample : System.Web.UI.Page
{
    private static string path = @"C:\Users\seongsoo\Documents\Visual Studio 2015\Projects\AspSample\AspSample\pos\";
    protected void Page_Load(object sender, EventArgs e)
    {
        ILog startLog = LogManager.GetLogger("Start Program");
        startLog.Debug("Program started!");
    }

    [WebMethod]
    public static void SavePos(string name, string x, string y)
    {
        ILog posLog = LogManager.GetLogger("Save Position");
        posLog.Debug(name + " -> x = " + x + ", y = " + y + " clicked!");
    }

    [WebMethod]
    public static void ResetPos(string name)
    {
        ILog posLog = LogManager.GetLogger("Reset Position");
        posLog.Debug(name + "-> Reset!");
    }

    [WebMethod]
    public static void SaveData(string name, string pos)
    {
        ILog dataLog = LogManager.GetLogger("Save Data");
        try
        {
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (dirInfo.Exists == false)
            {
                if (dirInfo != null)
                    dirInfo.Create();
            }
            using (StreamWriter writer = new StreamWriter(new FileStream(path + name + ".txt", FileMode.Create)))
            {
                writer.WriteLine(pos);
                dataLog.Debug(name + "-> Success!");
            }
        }
        catch (Exception e)
        {
            dataLog.Error(e.Message);
        }
    }
}