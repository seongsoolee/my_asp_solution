﻿my_img = [];
my_img_idx = 0;
my_img_pos = [];
my_img_pos_idx = 0;

function imgFileSelection(event) {
    my_img = event.target.files;
    document.getElementById('myImg').src = window.URL.createObjectURL(my_img[0]);
}

function imgFileNext() {
    if (my_img_idx < my_img.length - 1) {
        document.getElementById('myImg').src = window.URL.createObjectURL(my_img[++my_img_idx]);
        my_img_pos_idx = 0;
    }
    else {
        alert("Last Image!");
        $('#nextBtn').attr('disabled', true);
    }
}

function imgFileReset() {
    my_img_pos = [];
    my_img_pos_idx = 0;
}

function getImgName() {
    return my_img[my_img_idx].name;
}

function getImgPosStr() {
    var my_img_pos_str = '';
    for (var i = 0; i < my_img_pos.length; i++) {
        my_img_pos_str += my_img_pos[i].X + ' ' + my_img_pos[i].Y + '\r\n';
    }
    my_img_pos = [];

    return my_img_pos_str;
}

//https://stackoverflow.com/questions/502366/structs-in-javascript 참고
function makePosStruct(pos) {
    var pos = pos.split(' ');
    function construct() {
        for (var i = 0; i < pos.length; i++) {
            this[pos[i]] = arguments[i];
        }
    }
    return construct;
}

function imgPositionPush(event) {
    pos = getImgPos(event);
    my_img_pos.push(pos);
}

function getImgPos(event) {
    var pos_struct = makePosStruct('X Y');
    var pos = new pos_struct(event.offsetX, event.offsetY);
    return pos;
}