﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sample.aspx.cs" Inherits="sample" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
    <script src="sample.js"></script>
    <script>
        $(document).ready(function () {
            $('#fileSelector').bind('change', imgFileSelection);
            $('#resetBtn').click(function () {
                var name = getImgName();
                var data = { 'name': name };
                var obj = JSON.stringify(data);
                $.ajax({
                    type: 'POST',
                    url: 'sample.aspx/ResetPos',
                    data: obj,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: imgFileReset
                });
            });
            $('#myImg').click(function () {
                var name = getImgName();
                var pos = getImgPos(window.event);
                var data = { 'name': name, 'x': pos.X, 'y': pos.Y };
                var obj = JSON.stringify(data);
                $.ajax({
                    type: 'POST',
                    url: 'sample.aspx/SavePos',
                    data: obj,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: imgPositionPush(window.event)
                });
            });
            $('#nextBtn').click(function () {
                var name = getImgName();
                var pos = getImgPosStr();
                var data = { "name": name, "pos": pos };
                var obj = JSON.stringify(data);
                $.ajax({
                    type: 'POST',
                    url: 'sample.aspx/SaveData',
                    data: obj,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: imgFileNext
                });
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <input type="file" id="fileSelector" multiple="multiple" accept="image/*" />
    <br />
    <img id="myImg" src="//:0" />
    <br />
    <input type="button" id="resetBtn" value="Reset" />
    <input type="button" id="nextBtn" value="Next" />
    <br />
    </div>
    </form>
</body>
</html>
